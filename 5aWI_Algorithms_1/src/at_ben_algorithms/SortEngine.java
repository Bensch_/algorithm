package at_ben_algorithms;

import at_ben_algorithms_sorter.SortAlgorithm;

public class SortEngine {

	private SortAlgorithm SortAlgorthm;

	public int[] sort(int[] data) {
		return SortAlgorthm.sort(data);
	}
	
	
	public void setAlgorithm(SortAlgorithm algo) {
		this.SortAlgorthm = algo;
	}
}
