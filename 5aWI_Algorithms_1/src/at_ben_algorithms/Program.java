package at_ben_algorithms;

import at.ben.algorithms.search.BinarySearch;
import at.ben.algorithms.search.sequencesearch;
import at_ben_algorithms_helper.DataGenerator;
import at_ben_algorithms_sorter.Bubblesorter;
import at_ben_algorithms_sorter.Selectionsorter;
import at_ben_algorithms_sorter.SortAlgorithm;
import at_ben_algorithms_sorter.Summe;

public class Program {

	public static void main(String[] args) {

		/* int[] numbers = DataGenerator.generateRandomData(10, 2, 17);

		DataGenerator.sort(numbers);

		DataGenerator.printData(numbers); */
		
		/*SortAlgorithm bs = new Bubblesorter();
		SortAlgorithm ss = new Selectionsorter();
		SortEngine se = new SortEngine();
		se.setAlgorithm(bs);
		int[] numbers = se.sort(DataGenerator.generateRandomData(17, 1, 80));
		
		DataGenerator.printData(numbers);
		*/
		
		
		/* Summe rechnen
		Summe sum = new Summe();
		sum.summe(36, 34);
		*/
		
		
		/* Sequencesearch
		sequencesearch sq = new sequencesearch();
		int[] numbersArray = { 3, 17, 19 , 3, 7, 100};
		System.out.println("Die Nummer ist an der" + " " + sq.searchnumber(numbersArray, 100) +  ".Stelle");
		*/
		
		BinarySearch bs = new BinarySearch();
		int[] arr = {2, 4, 6, 8, 10, 12, 14, 16};
		bs.searchNumberbyBinarySearch(4, arr);

	}

}
