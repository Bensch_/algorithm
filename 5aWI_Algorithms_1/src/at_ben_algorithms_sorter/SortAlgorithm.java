package at_ben_algorithms_sorter;

public interface SortAlgorithm {
	
	public int[] sort(int[] data);
	public String getName();
}
