package at_ben_algorithms_sorter;

public class Selectionsorter implements SortAlgorithm {

	public int[] sort(int[] RandomArray) {
		
		int tempData;
		for (int i = 1; i < RandomArray.length; i++) {
			tempData = RandomArray[i];
			int j = i;
			while (j > 0 && RandomArray[j - 1] > tempData) {
				RandomArray[j] = RandomArray[j - 1];
				j--;
			}
			RandomArray[j] = tempData;
		}
		return RandomArray;
	}

	public String getName() {
		return "Selectionsorter";
	}
}
