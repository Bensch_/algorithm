package at_ben_algorithms_sorter;

public class Bubblesorter implements SortAlgorithm {

	public  int[] sort(int[] RandomArray) {
		int tempData;
		for (int i = 0; i < RandomArray.length; i++) {
			for (int j = RandomArray.length - 1; j > 0; j--) {
				if (RandomArray[j - 1] > RandomArray[j]) {
					tempData = RandomArray[j];
					RandomArray[j] = RandomArray[j - 1];
					RandomArray[j - 1] = tempData;
				}
			}
		}
		return RandomArray;
	}

	public String getName() {
		return "Bubblesorter";
	}
}
