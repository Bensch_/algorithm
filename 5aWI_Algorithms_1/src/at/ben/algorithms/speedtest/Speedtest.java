package at.ben.algorithms.speedtest;

import java.util.ArrayList;
import java.util.List;

import at_ben_algorithms_helper.DataGenerator;
import at_ben_algorithms_sorter.Bubblesorter;
import at_ben_algorithms_sorter.Selectionsorter;
import at_ben_algorithms_sorter.SortAlgorithm;

public class Speedtest {
	private List<SortAlgorithm> algorithms;

	public Speedtest() {
		this.algorithms = new ArrayList<SortAlgorithm>();
	}

	public void addAlgorithm(SortAlgorithm algo) {
		this.algorithms.add(algo);
	}

	public void run() {
		int[] data = DataGenerator.generateRandomData(9, 10, 100000);
		for (SortAlgorithm sort : algorithms) {
			System.out.println(sort.getName());
			long start = System.nanoTime();    
			sort.sort(data);
			long timeNeeded = System.nanoTime() - start;
			System.out.println(timeNeeded);
			// DataGenerator.printData(data);
		}
	}
	
	public static void main(String[] args) {
		
		Bubblesorter bs = new Bubblesorter();
		Selectionsorter ss = new Selectionsorter();
		Speedtest st = new Speedtest();
		st.addAlgorithm(bs);
		st.addAlgorithm(ss);
		st.run();
		
	}

}
