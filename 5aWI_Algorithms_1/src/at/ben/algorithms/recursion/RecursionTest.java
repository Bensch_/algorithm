package at.ben.algorithms.recursion;

public class RecursionTest {
	
	
	public static void main(String[] args) {
		System.out.println(RecAdd(17,1));
	}

	private static int add (int number){
		int sum = 0;
		while (sum < 100){
			sum += number;
		}
		return sum;
	}
	
	private static int RecAdd (int number, int counter){
		if (counter >= 3){
			return number;
		}
		else {
			return number + RecAdd(number, ++counter);
		}
	}
	
}
