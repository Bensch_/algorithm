package at.ben.algorithms.recursion;

public class Hexnumber {
	public static void main(String[] args) {
		printHexnumber(3494);
	}
	
	private static void printHexnumber(int number){
		
		int divide = number % 16;
		
		if (number <= 16){
			System.out.println(Integer.toHexString(number % 16));
		}
		else{
			System.out.println(divide + (printHexnumber(number / 16)));

		}
	}
}
