package at.ben.algorithms.recursion;

public class GGT {
	
	public static void main(String[] args) {
		System.out.println(ggt(17,3));
	}
	
	
	private static int ggt (int a, int b){
		
		System.out.println(a + ":" + b);
		
		if (a == b){
			return a;
		}
		else if (a > b){
			return ggt(a-b,b);
		}
		else{
			return ggt(a, b-a);
		}
	}
	
}
