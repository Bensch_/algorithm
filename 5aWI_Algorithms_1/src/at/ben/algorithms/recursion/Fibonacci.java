package at.ben.algorithms.recursion;

public class Fibonacci {
	
	public static void main(String[] args) {
		System.out.println(fibonacci(6));
	}
	
	private static int fibonacci(int x) {
		if (x == 1 || x == 2) {
			return 1;
		} else {

			return fibonacci(x - 1) + fibonacci(x - 2);
		}
	}
}
