package at.ben.algorithms.recursion;

public class Fakult�t {
	
	public static void main(String[] args) {
		System.out.println(getFactorial(6));
	}
	
	private static int getFactorial(int number){
		if (number ==1){
			return 1;
		}
		else{
			return number * getFactorial(number - 1);
		}
	}
}
