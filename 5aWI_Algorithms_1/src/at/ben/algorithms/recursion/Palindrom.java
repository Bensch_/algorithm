package at.ben.algorithms.recursion;

public class Palindrom {

	public static void main(String[] args) {
		System.out.println(isPalindrom("reliefpfeiler"));
		System.out.println(isPalindrom("maoam"));
	}

	private static int isPalindrom(String palindrom) {

		if (palindrom.length() == 0 || palindrom.length() == 1)
			return 1; // Wenn es ein Palindrom ist

		if (palindrom.charAt(0) == palindrom.charAt(palindrom.length() - 1))
			return isPalindrom(palindrom.substring(1, palindrom.length() - 1));

		else { // Wenn es keines ist
			return 2;
		}
	}
}
