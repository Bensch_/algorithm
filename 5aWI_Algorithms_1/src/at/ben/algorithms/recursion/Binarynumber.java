package at.ben.algorithms.recursion;

public class Binarynumber {
	public static void main(String[] args) {
		System.out.println(printBinarynumber(217));
	}
	
	private static int printBinarynumber(int number){
		
		int divide = number%2;
		
		if (number == 0 || number == 1){
			return number;
		}
		else{
			return divide + (printBinarynumber(number/2)*10);
		}
	}
}
