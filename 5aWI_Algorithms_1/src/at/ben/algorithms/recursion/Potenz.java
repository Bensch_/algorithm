package at.ben.algorithms.recursion;

public class Potenz {
	public static void main(String[] args) {
		System.out.println(getPotenz(5, 2));
	}
	
	private static int getPotenz(int number, int potenz){
		if (potenz == 1){
			return number;
		}
		else{
			return number * getPotenz(number, --potenz);
		}
	}
}
