package at.ben.algorithms.search;

import java.util.Iterator;

public class BinarySearch {
	public void searchNumberbyBinarySearch(int unkownNumber, int[] numbersArray) {

		int smallerMid = 0;
		int biggerMid = numbersArray.length - 1;

		while (smallerMid <= biggerMid) {
			int midNumber = (smallerMid + biggerMid) / 2;
			if (numbersArray[midNumber] == unkownNumber) {
				System.out.println("Die Zahl ist an der Stelle:" + midNumber);
				break;
			}

			// if (unkownNumber < numbersArray[midNumber]){
			// smallerMid = midNumber + 1;
			// return midNumber;
			// }

			if (unkownNumber > numbersArray[midNumber]) {
				biggerMid = midNumber - 1;
				midNumber = (smallerMid + biggerMid) / 2;

			} 
			else if (unkownNumber < numbersArray[midNumber]) {
				smallerMid = midNumber + 1;
				midNumber = (smallerMid + biggerMid) / 2;
				
			}
		}
	}
}
