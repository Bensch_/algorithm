package at.ben.datastructures.list;

public class Node {

	private int value;
	private Node next;
	private Node prev;

	

	public Node(int value) {
		super();
		value = this.value;
	}



	public int getValue() {
		return value;
	}



	public void setValue(int value) {
		this.value = value;
	}



	public Node getNext() {
		return next;
	}



	public void setNext(Node next) {
		this.next = next;
	}



	public Node getPrev() {
		return prev;
	}



	public void setPrev(Node prev) {
		this.prev = prev;
	}



}
